# MarXdown

MarXdown is --an attempt to knock down marxism-- a preprocessor for LaTeX inspired by Markdown. Requires Python 2.7.

## Description

Although LaTeX is the best typesetting system ever, most of the time, is it pretty hard to type compilable LaTeX code in one go. One often forgets the `$` delimiters (and replacing them with their oriented equivalents `\(` and `\)` would be tedious), or braces. Also, most LaTeX users use `<x, y>` as a dot product, which at the same time is incorrect and produces an unaesthetic result, the correct choice being: `\langle x, y \rangle`, rather painful to read and write.

MarXdown takes a file written in a lazy way (possibly with Greek letters) and without any `$`'s, tries to figure out what is math and what is not (using regexps), then adds the math delimiters accordingly.

## Usage

    python markdown.py sample.mxd

## Example

The following MarXdown code:

    I) Arithmétique

    Lm Le nombre de diviseurs de n = p_1^α_1 … p_r^α_r est d(n) = ∏_{i = 1}^n (α_i + 1).

    Prop \forall n \in \N, d(n) est impair <=> n est un carré parfait.

    II) Orthogonal d'un sous-espace

    Soit F une partie d'un espace euclidien E.

    def On appelle *orthogonal* de F l'espace F^\bot = \{x \in E, \forall y \in F, <x, y> = 0\}.

    III) Plans projectifs sur les corps finis

    Thm Pour tout q puissance de nombre premier, il existe un ensemble de cartes à q + 1 symboles parmi q^2 + q + 1 symboles possibles tel que deux cartes quelconques ont **exactement** un symbole en commun.

produces the following TeX file:

    \section*{I) Arithmétique}

    \paragraph{Lemme.} Le nombre de diviseurs de \( n = p_1^{\alpha_1} \ldots p_r^{\alpha_r} \) est \( d(n) = \prod_{i = 1}^n (\alpha_i + 1). \) 

    \paragraph{Proposition.} \( \forall n \in \N, d(n) \) est impair \( \Leftrightarrow n \) est un carré parfait.

    \section*{II) Orthogonal d'un sous-espace}

    Soit \( F \) une partie d'un espace euclidien \( E. \) 

    \paragraph{Définition.} On appelle \emph{orthogonal} de \( F \) l'espace \( F^\bot = \{x \in E, \forall y \in F, \langle x, y \rangle = 0\}. \) 

    \section*{III) Plans projectifs sur les corps finis}

    \paragraph{Théorème.} Pour tout \( q \) puissance de nombre premier, il existe un ensemble de cartes à \( q + 1 \) symboles parmi \( q^2 + q + 1 \) symboles possibles tel que deux cartes quelconques ont \textbf{exactement} un symbole en commun.

## TODO

* `->^` : `\xrightarrow`
