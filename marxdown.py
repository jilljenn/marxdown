# coding=utf8
import re, sys
from collections import Counter

code = open('marxdown.tex').read()

DEBUG = True

replaces = {
	'α': r'\\alpha',
	'β': r'\\beta',
	'δ': r'\delta',
	'ε': r'\\epsilon',
	'φ': r'\\varphi',
	'λ': r'\\lambda',
	'μ': r'\\mu',
	'ν': r'\\nu',
	'σ': r'\\sigma',
	'τ': r'\\tau',
	'θ': r'\\theta',
	'χ': r'\chi',
	'∆': r'\Delta',
	'∑': r'\\sum',
	r'∏(_{. = .}\^)': r'\prod\1',
	r'∏(_[^{])': r'\Pi\1',
	r'∏(_{[^=≤≥]+})': r'\Pi\1',
	'ψ': r'\psi',
	'ω': r'\\omega',
	'…': r'\ldots',
	'≠': r'\\neq',
	'≤': r'\leqslant',
	'≥': r'\\geqslant',
	'<-': r'\leftarrow',
	'->': r'\\rightarrow',
	'<=>': r'\Leftrightarrow',
	'<= ': r'\Leftarrow ',
	' =>': r' \Rightarrow',
	r' \\ ': r' \\setminus ',
	'·': r'\\cdot ',
	'×': r'\\times ',
	'BON': r'base orthonormée',
	'_ij': r'_{ij}',
	'_pq': r'_{pq}',
	'ker': r'\\ker',
	'det': r'\\det',
	'dim ': r'\\dim ',
	r'exp([^a-z])': r'\\exp \1',
	'd°': r'\\deg',
	r'<([^<>]*),([^<>]*)>': r'\\langle \1,\2 \\rangle',
	r'<([A-Za-z]+)>': r'\\langle \1 \\rangle',
	r'o\+': r'\\oplus',
	r'_([^ ]+)\^([^ {}]+)_([^ ,]+)': r'_\1^{\2_\3}',
	r'\+_': r'\\sum_',
	r'\^-1': r'^{-1}',
	r'\{0\}': r'\{0\}'
}

replaces2 = {
	r'\*([A-Za-zéœ ]+)\*': r'\\emph{\1}',
	r'\*\*([A-Za-zéœ ]+)\*\*': r'\\textbf{\1}',
	r'1er ': r'1\\ier{} ',
	r'1er([^ ])': r'1\\ier\1',
	r' ([A-Za-z])-([a-z]+)': r' \( \1 \)-\2',
	r'^Rq': r'\\paragraph{Remarque.}',
	r'^Pb': r'\\paragraph{Problème.}',
	r'^Rqdef': r'\\paragraph{Remarque et définition.}',
	r'^Rqs': r'\\paragraph{Remarques.}',
	r'^dm': r'\\paragraph{Démonstration.}',
	r'^Prop': r'\\paragraph{Proposition.}',
	r'^Propdef': r'\\paragraph{Proposition et définition.}',
	r'^Thm': r'\\paragraph{Théorème.}',
	r'^Cor': r'\\paragraph{Corollaire.}',
	r'^def': r'\\paragraph{Définition.}',
	r'^defs': r'\\paragraph{Définitions.}',
	r'^ex': r'\\paragraph{Exemple.}',
	r'^Lm': r'\\paragraph{Lemme.}',
	r'^/!\\': r'\\paragraph{Attention.}',
	r'^([IVX]+\) .*)\\\\$': r'\\section*{\1}',
	r'^\\\\$': r''
}

mathtokens = ['+', '-', '(', ')', '=', '', 'am', 'a\'m']
mathmode = ['_', '\\', '^', '<', '>', '{', '}']
textmode = ['«', '»', '/!\\']

if __name__ == '__main__':
	filename = sys.argv[1] if len(sys.argv) > 1 else 'test.mxd'
	body = open(filename).read() # HALP

	c = Counter()
	unknown = Counter()
	grid = []
	words = []
	to_do = []
	for line in body.split('\n'):
		if line and (line[0] == '%' or line[:2] == '\\['):
			words.append([line])
			grid.append([False])
			continue
		for to_replace in replaces:
			line = re.sub(to_replace, replaces[to_replace], line)
		words.append(line.split(' '))
		is_math = []
		for token in line.split(' '):
			if not token:
				is_math.append(False)
			elif (token == line.split(' ')[0] and re.match('[IVXivx]\)', token)) or (re.match(r'\t*\*?\(?[A-Za-zéêàâùôç]+\'?-?[A-Za-zéèêàâùô]+\*?,?\)?\.?$', token) and not token in mathtokens) or any([symbol in token for symbol in textmode]):
				c['francais'] += 1
				is_math.append(False)
			elif re.search('\[[A-Za-z]\]', token) or re.search('\([A-Za-z]\)', token) or re.match(r'-?\(?{?[A-Za-z0-9]}?\)?,?\.?\'?$', token) or token in mathtokens or any([symbol in token for symbol in mathmode]):
				c['math'] += 1
				is_math.append(True)
			else:
				c['none'] += 1
				unknown[token] += 1
				to_do.append((len(grid), len(is_math)))
				is_math.append(None)
		grid.append(is_math)

	if DEBUG:
		# print unknown
		for token, _ in unknown.most_common():
			print token
		print len(unknown.keys()), 'inconnus'
		"""print c
		print grid
		print words"""

	for _ in range(2):
		for i, j in to_do:
			if (j > 0 and grid[i][j - 1]) or (j < len(grid[i]) - 1 and grid[i][j + 1]):
				grid[i][j] = True
	for i, j in to_do:
		if not grid[i][j]:
			grid[i][j] = False

	latex = ""
	for i, line in enumerate(grid):
		n = len(line)
		k = 0
		while k < n:
			bra = n if True not in line[k:] else line.index(True, k)
			latex += ' '.join(words[i][k:bra])
			ket = n if False not in line[bra:] else line.index(False, bra)
			if bra < n:
				latex += ' \\( ' + ' '.join(words[i][bra:ket]) + ' \\) '
			k = ket
		latex += '\\\\\n'

	for to_replace in replaces2:
		latex = re.sub(to_replace, replaces2[to_replace], latex, flags=re.M)

	code = code.replace('{{ marxdown }}', latex.replace('\\\\\n\n', '\n\n'))

	with open(filename.replace('mxd', 'tex'), 'w') as f:
		f.write(code)
